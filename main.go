package main

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"time"
)

type myTheme struct {
	fyne.Theme
}

func (m myTheme) Font(s fyne.TextStyle) fyne.Resource {
	if s.Monospace {
		return theme.TextFont()
	}
	return m.Theme.Font(s)
}

func main() {
	myApp := app.NewWithID("stopwatch")
	myApp.Settings().SetTheme(&myTheme{myApp.Settings().Theme()}) // apply the custom theme

	myWindow := myApp.NewWindow("Stopwatch")

	// Create a label to show the elapsed time
	elapsedTimeLabel := canvas.NewText("00:00:00", theme.ForegroundColor())
	elapsedTimeLabel.TextStyle = fyne.TextStyle{Monospace: true}
	elapsedTimeLabel.Alignment = fyne.TextAlignCenter
	elapsedTimeLabel.TextSize = 64 // Set your desired font size here

	stopwatch := NewStopwatch(elapsedTimeLabel)
	startStopButton := widget.NewButton("Start/Stop", func() {
		if stopwatch.running {
			stopwatch.Stop()
		} else {
			stopwatch.Start()
		}
	})

	resetButton := widget.NewButton("Reset", func() {
		stopwatch.Reset()
	})

	// Set up the layout
	buttons := container.NewHBox(startStopButton, resetButton)
	content := container.NewVBox(
		container.NewCenter(elapsedTimeLabel),
		container.NewCenter(buttons),
	)
	myWindow.SetContent(content)

	myWindow.Resize(fyne.NewSize(300, 150))
	myWindow.ShowAndRun()
}

type Stopwatch struct {
	running    bool
	elapsed    time.Duration
	ticker     *time.Ticker
	label      *canvas.Text
	startTime  time.Time
	stopTime   time.Time
	updateFunc func()
}

func NewStopwatch(label *canvas.Text) *Stopwatch {
	sw := &Stopwatch{
		label: label,
	}
	sw.updateFunc = sw.updateLabel
	return sw
}

func (sw *Stopwatch) Start() {
	sw.startTime = time.Now()
	sw.running = true
	sw.ticker = time.NewTicker(time.Second)

	go func() {
		for range sw.ticker.C {
			if sw.running {
				sw.updateFunc()
			}
		}
	}()
}

func (sw *Stopwatch) Stop() {
	if sw.running {
		sw.ticker.Stop()
		sw.stopTime = time.Now()
		sw.elapsed += sw.stopTime.Sub(sw.startTime)
		sw.running = false
		sw.updateLabel()
	}
}

func (sw *Stopwatch) Reset() {
	sw.Stop()
	sw.elapsed = 0
	sw.updateLabel()
}

func (sw *Stopwatch) updateLabel() {
	var totalElapsed time.Duration
	if sw.running {
		totalElapsed = time.Since(sw.startTime) + sw.elapsed
	} else {
		totalElapsed = sw.elapsed
	}
	sw.label.Text = fmt.Sprintf("%02d:%02d:%02d",
		int(totalElapsed.Hours()),
		int(totalElapsed.Minutes())%60,
		int(totalElapsed.Seconds())%60)
	sw.label.Refresh() // Refresh the label to update the text on screen
}
