# Tell me why?

I couldn't find a simple stopwatch that would run in the terminal or as widget on mac within 30 minutes, so i decided it will be faster to just write this funny readme and implement my own :D This is world of programming - you dont need super architecture to solve daily problems.

# Go Stopwatch Application

This project is a simple and fast stopwatch application written in Go. It exemplifies the kind of lightweight, performant applications Go is renowned for: rapid development, ease of deployment, and running efficiently on minimal hardware resources.

![Stopwatch Screenshot](Stopwatch.png)

## Overview

Go (or Golang) is known for its simplicity in syntax and its powerful standard library that supports concurrent operations and networking. This stopwatch application is a testament to the capabilities of Go to provide quick solutions to everyday problems, in this case, personal productivity management.

The application is a command-line interface (CLI) based stopwatch, ideal for developers or anyone who wants to track time spent on tasks, promoting efficiency and self-management.

## Features

- **Simple to Start**: With minimal setup, the stopwatch is ready to run immediately after cloning the repository.
- **User Interaction**: Easy commands to start, stop, and reset the timer.
- **Productivity Focused**: Designed to help users keep track of time spent on coding or any other productive activity without distractions.
- **Resource-Efficient**: Consumes very little system resources, ensuring that it can run in the background without affecting your main tasks.


## Getting Started

To run the stopwatch, you will need to have Go installed on your system. [Download Go](https://golang.org/dl/) if you haven't already.

### Installation

1. Clone the repository to your local machine:

   ```sh
   git clone https://github.com/donrezo/codingstopwatch.git
   ```
2. Navigate to the project directory:

   ```sh
   cd codingstopwatch
    ```
3. Run the application:

   ```sh
   go run main.go
   ```
### Usage
* The stopwatch starts automatically when the program runs.
* Enter stop to pause the timer.
* Enter start to resume the timer.
* Enter reset to reset the timer to zero.
* Enter exit to close the application.

### License
Distributed under the MIT License.